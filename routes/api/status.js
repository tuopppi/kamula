var tv4 = require("tv4");

var db = require("./db.js");
var utils = require("./utils.js");

var statuses = db.statuses();

var statusDefinitions = {
	text: {
		type: "string",
		pattern: "^.{1,200}$",
	},
	id: {
		type: "string",
		pattern: "^[0-9a-f]{24}$",
	}
};

/*
* POST add status message
* body
* {
*    text: "message",
*    comment_to: <id> (optional)
* }
*/
exports.add = function(req, res) {

	var requestSchema = {
		properties: {
			text: { $ref: "#/definitions/text" },
			comment_to: { $ref: "#/definitions/id" },
		},
		required: ['text'],
		additionalProperties: false,
		definitions: statusDefinitions,
	};

	var status = req.body;

	if(!tv4.validate(status, requestSchema)) {
		utils.sendValidationError(res, tv4.error);
		return;
	};

	status.created_at = new Date().toJSON();
	status.user = utils.getAuthenticatedUser(req);
	status.comment_to = status.comment_to || null;

	var success = function(result) {
		res.json(result[0]);
	};

	statuses.add(status, success, utils.createErrorHandler(res));

};

/*
* GET show status message
*/
exports.show = function(req, res) {
	
	var id = req.params[0];

	var success = function(result) {
		if(result.length === 0) {
			res.json(404, {error: "Id was not found"});
		} else {
			res.json({
				status: result[0] || null,
				comments: result.slice(1)
			});
		}
	};

	statuses.get(
		id, 
		success, 
		utils.createErrorHandler(res)
	);

};

/*
* DELETE remove status message
*/
exports.remove = function(req, res) {
	
	var user = utils.getAuthenticatedUser(req);
	var id = req.params[0];

	var idWasFound = function(result) {
		if(result.length === 0) {
			res.json(404, {
				id: id, 
				user: user, 
				error: "No matching id was found"
			});
		} else {
			if(result[0].user !== user) {
				utils.sendUnauthorizedAccess(res);
			} else {
				statuses.remove(
					id, user,
					utils.createSuccessHandler(res), 
					utils.createErrorHandler(res)
				);
			}
		}
	};

	statuses.get(id, idWasFound, utils.createErrorHandler(res));

};

/*
* GET status message timeline
* params 
* limit: take <n> most recent status messages (0 = no limit)
* user: filter result by username
*       multiple users can be separated by comma
*/
exports.timeline = function(req, res) {

	var limit = parseInt(req.query.limit) || 0;
	var users = req.query.user && req.query.user.split(',');

	statuses.list(
		users, limit, 
		utils.createSuccessHandler(res), 
		utils.createErrorHandler(res)
	);

};
