var ObjectID = require('mongodb').ObjectID;
var crypto = require('crypto');

exports.ERROR_CODES = {
	DUPLICATE_KEY: 11000
};

var db;

var resultHandler = function(success, error) {
	return function(err, result) {
		err ? error(err) : success(result);
	}
}

exports.init = function(connection) {

	console.log("Database connection initialized")
	db = connection;

	// usernames must be unique in database
	db.collection('users').ensureIndex(
		{ user: 1 }, 
		{ unique: true }, 
		function handleError(err, indexName) {
			if(err) {
				console.log("MongoError: Could not make usernames unique");
				console.log(err.err);
				process.exit(1);
			}
		}
	);

}

exports.statuses = function() {
	
	that = {};

	that.list = function (users, limit, success, error) {
		
		var query = { comment_to: null };
		if(users != undefined) {
			query.user = { $in: users };
		}

		db.collection('statuses').find(query).limit(limit).sort('created_at', 'desc')
		.toArray(resultHandler(success, error));

	}

	that.get = function (id, success, error) {

		var cursor = db.collection('statuses').find({ 
			$or: [ { _id: ObjectID(id) }, 
				   { comment_to: ObjectID(id) } ] 
		})
		.sort('created_at', 'asc')
		.toArray(resultHandler(success, error));

	}

	that.add = function (status, success, error) {

		var ifUserFound = function(result) {
			status.name = result.name;
			status.comment_to = status.comment_to ? 
				ObjectID.createFromHexString(status.comment_to) : null;

			db.collection('statuses').insert(
				status, 
				{ w: 1 }, 
				resultHandler(success, error)
			);
		};

		db.collection('users').findOne(
			{ user: status.user }, 
			{ name: 1 }, 
			resultHandler(ifUserFound, error)
		);

	}

	that.remove = function(id, user, success, error) {

		db.collection('statuses').remove(
			{'_id': ObjectID(id), 'user': user }, 
			{ single: true }, 
			resultHandler(success, error)
		);

	}

	return that;	

}

exports.users = function() {

	// Use username as salt, not the best salt but good enough for this app
	var hashedPassword = function(password, salt) {
		var SECRET = "0(4~R4smjNC=@dBZEB-RcJ,#Jsw03^NXH-KAZLc3KAk(6i9$8LTvYotg4"
		var shasum = crypto.createHash('sha1');
		shasum.update(password + SECRET + salt, 'utf8')
		return shasum.digest('hex');
	}
	
	var users = {};

	users.auth = function(user, password, callback) {

		db.collection('users').count(
			{
				'user': user, 
				'password': hashedPassword(password, user) 
			}, 
			function(err, count) {
				var success = count === 1;
				callback(err, success);
			}
		);

	}

	users.list = function(success, error) {
		
		// return only if account is active
		// "password" : { "$exists" : true }

		var cursor = db.collection('users').find({}, { user: 1 });
		cursor.toArray(resultHandler(success, error));

	}

	users.get = function(user, success, error) {

		// Returns null as result if user is not found
		db.collection('users').findOne(
			{ user: user }, 
			{ password: 0 }, 
			resultHandler(success, error)
		);	

	}

	users.create = function(userAttributes, success, error) {
		
		userAttributes.friends = userAttributes.friends || [];
		userAttributes.password = hashedPassword(userAttributes.password, userAttributes.user);
		
		db.collection('users').insert(
			userAttributes, 
			{ w: 1 }, 
			resultHandler(success, error)
		);

	}

	users.update_info = function(user, data, success, error) {
		
		var values = {};

		if(data.name) {
			values.name = data.name;
		}

		if(data.email) {
			values.email = data.email;
		}

		if(data.friends) {
			values.friends = data.friends;
		}

		db.collection('users').update(
			{ 'user': user }, 
			{ $set: values }, 
			function(err, result) {
				err ? error(err) : users.get(user, success, error);
			}
		);

	}

	users.change_password = function(user, oldpw, newpw, success, error) {
		
		hashedOldpw = hashedPassword(oldpw, user);
		hashedNewpw = hashedPassword(newpw, user);

		db.collection('users').update(
			{ 'user': user, 'password': hashedOldpw }, 
			{ $set: { password: hashedNewpw } },
			resultHandler(success, error)
		);

	}

	users.remove = function(user, success, error) {

		// order of execution: "from bottom to top"

		/*
		var statusesRmSuccess = function() {
			// remove deleted user from other users friends list
			db.collection('users').update(
				{ 'friends': user }, 
				{ "$pull": { 'friends': user } },
				resultHandler(success, error)
			);
		}

		var userRmSuccess = function() {
			// remove deleted users status messages
			db.collection('statuses').remove(
				{ 'user' : user }, 
				{ w: 1 }, 
				resultHandler(statusesRmSuccess, error)
			);
		}
		*/
		
		// prevent user from loggin in again
		db.collection('users').update(
			{ 'user': user }, 
			{ "$unset" : { 'password' : "" } }, 
			resultHandler(success, error)
		);

	}

	return users;

}
