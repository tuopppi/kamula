exports.getAuthenticatedUser = function(req) {
  if(req.auth && req.auth.username) {
    return req.auth.username;
  }
  else if(req.session && req.session.username)
  {
    return req.session.username;
  } else {
    return null;
  }
}

exports.createErrorHandler = function(res) {
  return function(error) {
    res.json(400, {
      error: error.message
    });
  }
}

exports.createSuccessHandler = function(res) {
  return function(result) {
    res.json(result);
  }
}

exports.sendValidationError = function(res, error) {
	res.json(400, {
		error: error.message, 
	});
};

exports.sendUnauthorizedAccess = function(res) {
	res.json(403, { error: "Unauthorized access" });
};