var tv4 = require("tv4");
var db = require("./db.js");
var utils = require("./utils.js");

var users = db.users();

var userDefinitions = {
	user: {
		type: "string",
		not: { 
			enum: ['settings'],
		},
		pattern: "^[A-Za-z0-9]{1,30}$",
	},
	name: {
		type: "string",
		pattern: "^[\\S ]{1,100}$",
	},
	email: {
		type: "string",
		pattern: "^\\S+@\\S+$",
	},
	password: {
		type: "string",
		pattern: "^\\S{6,}$",
	},
	friends: {
		type: "array",
		items: {
            $ref: "#/definitions/user",
        },
        uniqueItems: true,
	},
};

/* 
 * GET list of all users
 */
exports.list = function(req, res) {
	
	users.list(utils.createSuccessHandler(res), utils.createErrorHandler(res));

};

/*
 * GET info about specific user
 */
exports.info = function(req, res) {
	
	var user = req.params.username || req.params[0];

	var success = function(result) {
		if(result) {
			res.json(result);
		} else {
			res.json(404, { error: "User not found" });
		}
	}

	users.get(user, success, utils.createErrorHandler(res));

};

/*
 * POST add new user
 */
exports.create = function(req, res) {

	var requestSchema = {
		properties: {
			user: { $ref: "#/definitions/user" },
			name: { $ref: "#/definitions/name" },
			email: { $ref: "#/definitions/email" },
			password: { $ref: "#/definitions/password" },
			friends: { $ref: "#/definitions/friends" }
		},
		required: ['user', 'name', 'email', 'password'],
		additionalProperties: false,
		definitions: userDefinitions,
	};

	if(!tv4.validate(req.body, requestSchema)) {
		utils.sendValidationError(res, tv4.error);
		return;
	}

	var success = function(result) {
		var created = result[0];
		req.session.username = created.user; // login
		var profilePage = '/#' + created.user;
		res.redirect(201, profilePage);
	};

	var error = function(error) {
		if(error.code == db.ERROR_CODES.DUPLICATE_KEY) {
			res.json(403, { error: "User already exists" });
		} else {
			res.json(400, { error: error.name });
		}
	};

	users.create(req.body, success, error);

};

/*
 * PUT update user info
 * 
 * You can change name and email fields. User field cannot be changed!
 * 
 * If you want to change your password the message body must contain your current password
 * and new_password fields. Same request can also include other fields
 * you want to update. (Look example above)
 */
exports.update = function(req, res) {

	var requestSchema = {
		properties: {
			_id: {},
			user: { $ref: "#/definitions/user" },
			name: { $ref: "#/definitions/name" },
			email: { $ref: "#/definitions/email" },
			friends: { $ref: "#/definitions/friends" },
			password: { $ref: "#/definitions/password" },
			new_password: { $ref: "#/definitions/password" },
		},
		additionalProperties: false,
		definitions: userDefinitions,
	};

	var attributes = req.body;
	var target_username = req.params.username;
	var authenticated_user = utils.getAuthenticatedUser(req);
	var isChangingPassword = attributes.new_password && attributes.password;

	if(!tv4.validate(attributes, requestSchema)) {
		utils.sendValidationError(res, tv4.error);
		return;
	} 

	if(authenticated_user !== target_username) {
		utils.sendUnauthorizedAccess(res);
		return;
	}

	var updateAttributes = function(passwordOk) {
		if(passwordOk === 1) {
			users.update_info(
				authenticated_user, 
				attributes, 
				utils.createSuccessHandler(res), 
				utils.createErrorHandler(res)
			);
		} else {
			utils.sendUnauthorizedAccess(res);
			return;
		}
	};

	if(isChangingPassword) {
		users.change_password(
			authenticated_user, 
			attributes.password, 
			attributes.new_password, 
			updateAttributes, // if successful, change rest of attributes
			utils.createErrorHandler(res));
	} else {
		updateAttributes(1);
	}

};

/*
 * DELETE update user info
 */
exports.remove = function(req, res) {

	var authenticated_user = utils.getAuthenticatedUser(req);
	var target_username = req.params.username;

	if(target_username !== authenticated_user) {
		utils.sendUnauthorizedAccess();
		return;
	}

	users.remove(
		authenticated_user, 
		utils.createSuccessHandler(res), 
		utils.createErrorHandler(res)
	);

};