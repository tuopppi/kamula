var db = require('./api/db.js');

/* GET / */
exports.index = function(req, res) {
  res.render('front', { 
  	username: req.session.username || null,
    error: req.session.error || "",
  });
  delete req.session.error;
};

/* POST /login */
exports.login = function(req, res) {
  var callback = function(err, result) {
    if(!err && result) {
      // store username to session, so that we can identify the user
      // in following requests
      req.session.username = req.body.username;
    } else {
      req.session.error = "Wrong username or password";
    }
    res.redirect('/');
  }

  db.users().auth(req.body.username, req.body.password, callback);
}

/* GET /logout */
exports.logout = function(req, res) {
  req.session.destroy();
  res.redirect('/');
};