var express = require('express');
var db = require('./routes/api/db.js');
var config = require('./config.json');

// Authentication middleware (expressjs) for Kamula
exports.requireAuth = function(req, res, next) {
	if(req.session.username) {
		// User has already logged in, pass.
		//
		// For better security, we could include some key to session and keep those
		// keys in database. Keys in database would be changed periodically
		// so that user would have to login again. This would also invalidate old session 
		// cookies.
		next();
	} else {
		var defaultRealm = 'Kamula';
		
		// callback :: string -> bool -> IO()
		// callback error successfulAuth 
		express.basicAuth(function(user, pass, callback) {
			// Authenticate username and password against database
			db.users().auth(user, pass, callback);	
		}, config.realm || defaultRealm)(req, res, next);
	}
}