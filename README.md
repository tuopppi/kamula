# TIE-23500 Web-ohjelmointi harjoitustyö 2014 - Kamula
## Ryhmä: 52   |  Tuomas Vaherkoski

Työ testattu Node.js:n versiolla 0.10.15 ja MongoDB versiolla 2.4.6

# Yleistä

Työ on tehty luomalla REST-rajapinta kaikille tarvittaville operaatioille.
Asiakaspäässä käytetään Backbone.js kirjastoa REST-clientin toteutukseen. Backbone.js:n päälle tehty clientti löytyy tiedostosta `public/js/timeline.js`

# Käynnistys

1. Asenna tarvittavat paketit

        $ npm install

2. Aseta mongodb osoite config.json tiedostoon projektin juuressa

        { 
        "mongodb" : "mongodb://localhost:27017/kamula",
        "port" : "3000", 
        "realm": "Kamula"
        }

3. Käynnistä serveri 

        $ nodejs app.js

# Testit

* Testit on tehty jmeter ohjelmalle (kamula-test.jmx)
* Ubuntussa riittää "apt-get install jmeter" asentamat paketit
* Tietokannan pitää olla tyhjä ja serverin uudelleenkäynnistetty kun testit ajetaan, muuten tulee virheitä jo olemassa olevista käyttäjistä

# REST API

Tunnistautuminen tapahtuu HTTP basic:llä. Tunnukset ovat samat joilla 
Kamulaan on rekisteröidytty.

Jos kyselyssä tapahtuu virhe (4XX koodit) vastaus sisältää selityksen muodossa:
    
    { "error": "User not found" }

## Käyttäjät

**GET** /api/users
> Listaa kaikki järjestelmän käyttäjät.  
> Ei vaadi tunnistautumista.
>
>     [ { "user": "jesse",
>         "_id": "533dac26f88c01317f0486c7" },
>       { "user": "tuopppi",
>         "_id": "533db5fb4941af180f4d014b" } ] 

**POST** /api/users
> Lisää uusi käyttäjä. Ei vaadi tunnistautumista.  
> Salasanat tallennetaan tietokantaan suolattuna ja SHA1 hashattyna.    
> Viestin BODY osuus:
>
>     { "user" : "teemu",
>       "email" : "email@email.com",
>       "name" : "Teemu Teekkari",
>       "password" : "salasana" }
>
> #### Vastauskoodit:  
> 201: Käyttäjä luotiin  
> 400: Virheellinen pyyntö (esim puuttuva kenttä)  
> 403: Käyttäjä löytyy jo  

**GET** /api/users/{username}
> Palauttaa tiedot käyttäjästä. Ei vaadi tunnistautumista.
>
>      { "_id": "533dab42f88c01317f0486c6",
>        "email": "tuomasvaherkoski@local.com",
>        "friends": [ "jesse" ],
>        "name": "Tuomas Viherkoski",
>        "user": "tuopppi" }
>
> #### Vastauskoodit:  
> 200: Löytyi  
> 404: Käyttäjää ei löytynyt

**PUT** /api/users/{username}
> Päivittää käyttäjän tiedot.  Vaatii tunnistautumisen.  
> Salasanan päivitys (BODY): 

>     { "password": "vanhasalasana",
>       "new_password": "uusisalasana" }

> Muiden tietojen päivitys (BODY):

>     { "name" : "Uusi nimi",
>       "friends": ["jesse", "juuso"],
>       "email" : "uusi@osoite.com" }

> Kaikkia tietoja ei ole pakko päivittää kerralla, vaan mikä tahansa 
> edellisistä voi olla viestissä myös yksinään.  
>  
> #### Vastauskoodit:  
> 200: Tiedot päivitetty  
> 400: Virheellinen pyyntö (esim puuttuva kenttä)  
> 401: Tunnistautumaton käyttäjä  
> 403: Ei oikeuksia (väärät käyttäjätunnukset tai vanha salasana väärin)  
> 404: Käyttäjää ei löydy  

**DELETE** /api/users/{username}
> Poistaa käyttäjän. Vaatii tunnistautumisen.
>
> #### Vastauskoodit:  
> 200: Käyttäjä poistettu  
> 401: Tunnistautumaton käyttäjä  
> 403: Ei oikeuksia  
> 404: Käyttäjää ei löydy  

## Statusviestit

**GET** /api/statuses
> Palauttaa listauksen statusviesteistä. Ei vaadi tunnistautumista.
>
> ###Parametrit: 
>
> **limit** *(valinnainen)* 
> > rajoittaa n uusimpaan viestiin  
> > esimerkki: *2*
>
> **user** *(valinnainen)*
> > pilkulla eroteltu lista käyttäjistä joiden viestit halutaan listata  
> > esimerkki: *tuopppi,jesse*
>
> Esim  
>
>     /api/statuses?limit=2&user=tuopppi,jesse
>
>     [ { "text": "viesti",
>         "created_at": "2014-04-03T18:49:19.630Z",
>         "user": "tuopppi",
>         "comment_to": null,
>         "name": "Tuomas Viherkoski",
>         "_id": "533dad2ff88c01317f0486d1"
>       },
>       { "text": "jotain",
>         "created_at": "2014-04-03T18:46:24.621Z",
>         "user": "jesse",
>         "comment_to": null,
>         "name": "Testi",
>         "_id": "533dac80f88c01317f0486d0" } ]    

**POST** /api/statuses
> Lisää uusi statusviesti. Vaatii tunnistautumisen.  
> Viestin BODY:
>
>     { "text" : "viesti" }
> 
> tai
>
>     { "text" : "kommentti", "comment_to": "533dac80f88c01317f0486d0" } 
>
> #### Vastauskoodit:  
> 200: Onnistunut lisäys 
> 401: Tunnistautumaton käyttäjä  
> 400: Virheellinen pyyntö (esim puuttuva kenttä)  

**GET** /api/statuses/{id}
> Palauttaa viestin ja siihen liittyvät kommentit.
> Ei vaadi tunnistautumista.
>
>     { "status": {
>         "text": "viesti",
>         "created_at": "2014-04-03T18:49:19.630Z",
>         "user": "tuopppi",
>         "comment_to": null,
>         "name": "Tuomas Vaherkoski",
>         "_id": "533dad2ff88c01317f0486d1"
>       },
>       "comments": [] }
>
> #### Vastauskoodit:  
> 200: OK  
> 404: Viestiä ei löydy  

**DELETE** /api/statuses/{id}
> Poistaa statusviestin. Vaatii tunnistautumisen.
>
> #### Vastauskoodit:  
> 200: Poistettu onnistuneesti   
> 401: Tunnistautumaton käyttäjä  
> 403: Ei oma viesti  
> 404: Viestiä ei löydy  