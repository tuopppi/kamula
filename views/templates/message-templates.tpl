<script id="status-message-template" type="text/x-handlebars-template">
    <div class="list-group-item status status-orginal">

      <a class="h3" href="#{{status.user}}">{{status.name}}</a>
      <a class="h4" href="#{{status.user}}">@{{status.user}}</a>
      <span class="h5 timestamp">{{fromNow status.created_at}}</span>
      {{#if (isOwnComment status.user username)}}
        <a class="destroy" title="Remove message">
          <span class="glyphicon glyphicon-remove"></span>
        </a>
      {{/if}}

      <p>{{status.text}}</p>

      <a class="show-comments">
        {{#if username}} Show or add comments {{else}} Show comments {{/if}}
      </a>
      
    </div>
</script>

<script id="send-comment-template" type="text/x-handlebars-template">
    <div class="list-group-item add-comment">
      <input type="text" class="form-control" id="comment-text" placeholder="Reply to @{{user}}">
    </div>
</script>

<script id="comment-template" type="text/x-handlebars-template">
  <a class="h3" href="#{{user}}">{{comment.name}}</a>
  <a class="h4" href="/#{{user}}">@{{comment.user}}</a>
  <span class="h5 timestamp">{{fromNow comment.created_at}}</span>
  {{#if (isOwnComment comment.user username)}}
    <a class="destroy"><span class="glyphicon glyphicon-remove"></span></a>
    {{/if}}
  <p>{{comment.text}}</p>
</script>