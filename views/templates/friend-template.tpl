<script id="friend-template" type="text/x-handlebars-template">
	<h4>Friends</h4>
	{{#each friends}}
		<a class="label label-default" href="#{{user}}">{{user}}</a>
	{{else}}
		No friends
	{{/each}}

	{{#if signedInUser}}
	<button class="btn btn-default btn-xs" data-toggle="modal" data-target="#addFriendModal">
		Add friends
	</button>
	{{/if}}
</script>

<script id="all-users-template" type="text/x-handlebars-template">
	<h4>All users</h4>
	{{#each this}}
		<a class="label label-default" href="#{{user}}">{{user}}</a>
	{{/each}}
</script>

<script id="alert-template" type="text/x-handlebars-template">
	<strong>{{code}}</strong> {{message}}
</script>