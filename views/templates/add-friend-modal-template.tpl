<script id="friend-item-template" type="text/x-handlebars-template">
    {{#if isMyFriend}}
    <span class="glyphicon glyphicon-star"></span>
    {{else}}
    <span class="glyphicon glyphicon-star-empty"></span>
    {{/if}}
    {{user}}
</script>