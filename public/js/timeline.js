$(function(){

	/* Models */
	var User = Backbone.Model.extend({
		idAttribute: "user",
		urlRoot: "/api/users",
		defaults: {
			friends: []
		},	
		validate: function(attrs, options) {
			if (attrs.user == "settings" || attrs.user == undefined) {
				return "Invalid username";
			} else {
				return null;
			}
		},
	});

	var Status = Backbone.Model.extend({
		idAttribute: "_id",
		urlRoot: '/api/statuses',
		fetchComments: function(callback) {
			this.fetch({success: function(model, response, options) {
				callback(model.get("comments"));
			}, error: function()
			{
				callback([]);
			}});
		},
	});

	/* Collections */
	var UserList = Backbone.Collection.extend({
		url:"/api/users",
		model: User,
	});
	 
	var StatusMessages = Backbone.Collection.extend({
	    model: Status,    
	    limit: 0,
	    url: function() {
	    	var params = [];
	    	
	    	if(this.limit > 0) {
	    		params.push("limit=" + this.limit);
	    	}

	    	if(SelectedUser.get('user')) {
	    		var user = SelectedUser.get('user');
	    		var friends = SelectedUser.get('friends');
	    		if(friends) {
	    			params.push("user=" + user + "," + friends.join(","));
	    		} else {
	    			params.push("user=" + user);
	    		}
	    	} else if(SignedInUser.get('user')) {
	    		var user = SignedInUser.get('user');
	    		var friends = SignedInUser.get('friends');
	    		if(friends) {
	    			params.push("user=" + user + "," + friends.join(","));
	    		} else {
	    			params.push("user=" + user);
	    		}
	    	}

			return "/api/statuses?" + params.join("&");
	    },
	    comparator: 'created_at', 
	});
	
	/* Views */
	var TimelineView = Backbone.View.extend({
		el: '#timeline',
		initialize:function () {
	        this.listenTo(this.collection, 'reset', this.addAll);
	        this.listenTo(this.collection, 'add', this.addOne);
	        this.listenTo(this.collection, 'all', this.render);
	        this.listenTo(this.model, 'change', this.update);
	        this.listenTo(SignedInUser, 'change:friends', this.update);
	    },
	    update: function() {
	    	this.collection.fetch({reset:true});
	    },
	    render: function() {
	    	if(this.collection.length == 0) {
	    		this.$el.html("No messages");
	    	}
	    },
	    addOne: function(status) {
	    	if(status.get("comment_to") == null) {
		    	var view = new StatusView({model:status});
	    		this.$el.prepend(view.render().el);
	    	}
	    },
	    addAll: function() {
	    	this.$el.empty();
			this.collection.each(this.addOne, this);
	    },
	});

	var CommentView = Backbone.View.extend({
		tagName: "div",
		className: "list-group-item status status-comment",
		template: Handlebars.compile($('#comment-template').html()),
		events: {
	      "click a.destroy" : "remove",
	    },
	    render: function() {
	    	$(this.el).html(this.template({
        		comment: this.model.toJSON(), 
        		username: signedInUser
        	}));
	    	return this;
	    },
	    remove: function() {
	      	this.model.destroy();
	      	this.model.sync("delete", this.model);
	      	this.$el.remove();
	    },
	});

	var StatusView = Backbone.View.extend({
	 	template: Handlebars.compile($('#status-message-template').html()),
	 	addCommentTemplate: Handlebars.compile($('#send-comment-template').html()),
	 	tagName:"div",
	 	className:"list-group status-collection",
	    events: {
	      "click .status-orginal a.destroy" : "remove",
	      "click a.show-comments" : "loadComments",
	      "keypress div.add-comment" : "commentOnEnter",
	    },
		initialize: function() {
			this.listenTo(this.model, "reset", this.render);
			this.listenTo(this.model, "change:comments", this.renderComments);
	    },
	    render: function(eventName) {
	        $(this.el).html(
	        	this.template({
	        		status: this.model.toJSON(), 
	        		username: signedInUser
	        	})
	        );
	        return this;
	    },
	    renderComments: function() {
	    	$(this.el).find('.status-comment').remove();
	    	new StatusMessages(this.model.get("comments")).each(function(comment) { 
	    		$(this.el).append(new CommentView({model:comment}).render().el);
	    	}, this);
	    },
	    loadComments: function() {
	    	$(this.el).find("a.show-comments").hide();
	    	if(SignedInUser.get('user') != undefined) {
	    		$(this.el).append(this.addCommentTemplate(this.model.toJSON()));
	    	} else {
	    		$(this.el).addClass("expanded"); // add divider between status & comments
	    	}
	    	this.model.fetch();
	    },
	    // If you hit return in the comment input field
	    commentOnEnter: function(e) {
	    	var input = $(this.el).find("#comment-text");
			if (e.keyCode != 13) return;
			if (!input.val()) return;

			var comment = {
				comment_to: this.model.id,
				text: input.val(),
			};

			input.val('');

			var commented = this.model;
			Timeline.create(comment, {
				success: function(model) {
					commented.fetch(); // triggers renderComments function
				},
				error: function(model, res)
				{
					new AlertView().render(
						'alert-danger', 
						res.status, 
						res.responseJSON.error || "Error");
				}
			});
	    },
	    remove: function() {

	    	this.$el.remove();
	      	this.model.destroy();
	      	this.model.sync("delete", this.model);
	    },
	});

	var JoinView = Backbone.View.extend({
		el: "#joinModal",
		initialize: function() {
			_.bindAll(this, "createSuccess", "createError");
		},
		events: {
			"submit form": "joinKamula",
		},
		joinKamula: function(e) {
			e.preventDefault();
			
			var user = this.createNewUserFromData();
			user.save({}, {
				success: this.createSuccess,
				error: this.createError,
				wait: true,
			});
		},
		createNewUserFromData: function() {
			var user = new User({
				name: this.$el.find("#name").val(),
				email: this.$el.find("#email").val(),
				user: this.$el.find("#user").val(),
				password: this.$el.find("#password").val()
			});

			// Force creation of new User
			// User model uses user attribute as id and it is set automatically
			// defining id to undefined causes POST request, instead of PUT, which
			// would just try to update existing user.
			user.id = undefined;
			return user;
		},
		createSuccess: function(model, response, options) {
			// go to profile page and reload to get signed in user status
			window.location.href = response.getResponseHeader('Location');
			window.location.reload(true);
		},
		createError: function(model, response, options) {
			// 201 Created (Backbone.js thinks all but 200 is error...)
			if(response.status == 201) {
				this.createSuccess(model, response, options);
			} else {
				this.$el.find('.alert').remove();

				var err = $('<div class="alert alert-danger alert-dismissable">...</div>');
				err.html(response.responseJSON.error);
				this.$el.find('.modal-body').prepend(err);
			}
		},
	});

	var AlertView = Backbone.View.extend({
		tagName: 'div',
		className: 'alert',
		template: Handlebars.compile($('#alert-template').html()),
		attributes: {
			timeout: 5000,
		},
		// alert-success, alert-info, alert-warning, alert-danger
		render: function(type, code, message) {
			this.showMessage(type, code, message);
			return this;
		},
		flash: function(type) {
			var message = $("#error").find("span").attr("data");
			if(message) {
				this.showMessage(type, "Error", message);
			}
			return this;
		},
		showMessage: function(type, code, message) {
			var alert = $("#error");
			alert.append(this.$el);
			this.$el.addClass(type);
			this.$el.html(this.template({code: code, message: message})).show().delay(this.attributes.timeout);
			this.$el.fadeOut("fast", function() {
				$(this).remove();
			});
		}
	});

	var SendStatusMessageView = Backbone.View.extend({
		el: "#send-status-message",
		events: {
	      "keypress input#status-text" : "sendOnEnter",
	    },
	    sendOnEnter: function(e) {
	    	var input = $('#send-status-message').find("#status-text");
			if (e.keyCode != 13) return;
			if (!input.val()) return;

			Timeline.create({
				text: input.val(),
			}, {
				wait:true,
				success: function() {
					input.val('');
				},
				error: function(model, res) {
					new AlertView().render('alert-danger', res.status, res.responseJSON.error || "Error");
				}
			});
		},
		render: function (visible) {
			if(visible) {
				this.$el.show();
			} else  {
				this.$el.hide();
			}
		}
	});

	var UserListingView = Backbone.View.extend({
		el: "#friends",
	    initialize: function() {
	    	this.listenTo(this.model, "change", this.render);    	
	    	this.listenTo(AllUsers, "sync", this.render);

	    	if(this.model.get('user') != undefined) {
	    		this.model.fetch();
			}
	    },
		render: function () {
			if(signedInUser == undefined) {
				this.showAllUsers();
			} else {
				this.showFriends();
			}

			return this;
		},

		friendTemplate: Handlebars.compile($('#friend-template').html()),
		allUsersTemplate: Handlebars.compile($('#all-users-template').html()),

		showFriends: function() {
			if(this.model.get('friends') == undefined) {
				this.model.fetch();
			}

			var friends = new UserList(_.map(this.model.get('friends'), function(username) { 
				return new User({user:username});
			}, this));

			this.$el.html(this.friendTemplate({
				friends: friends.toJSON(),
				signedInUser: signedInUser
			}));
		},

		showAllUsers: function() {
			this.$el.html(this.allUsersTemplate(AllUsers.toJSON()));
		},
	});

	var FriendListItem = Backbone.View.extend({
		tagName: "li",
		className: "list-group-item friend",
		template: Handlebars.compile($('#friend-item-template').html()),
		events: {
			"click": "toggleFriendStatus",
		},
		initialize: function(attrs) {
			this.user = attrs.user;
		},
		toggleFriendStatus: function(e) {
			var friends = this.model.get('friends');
			var name = this.$el.text().trim();
			var isFriend = _.contains(friends, name);		
			var newFriends = [];

			if(isFriend) {
				newFriends = _.without(friends, name);
				this.$el.find('span').addClass("glyphicon-star-empty");
				this.$el.find('span').removeClass("glyphicon-star");
			} else {
				newFriends = friends.concat([name]);
				this.$el.find('span').addClass("glyphicon-star");
				this.$el.find('span').removeClass("glyphicon-star-empty");
			}

			this.model.set({friends: newFriends});
		},
		render: function() {
			this.$el.html(this.template({
				'user': this.user.get('user'), 
				'isMyFriend': _.contains(this.model.get("friends"), this.user.get('user'))
			}));
			return this;
		}

	})

	var FriendAddView = Backbone.View.extend({
		el: "#addFriendModal",
		initialize: function(data) {
			this.listenTo(this.collection, "sync", this.addAll);
			_.bindAll(this, "saveOnClose");
			this.$el.on("hide.bs.modal", this.saveOnClose);
		},

		saveOnClose: function(event) {
			this.model.save({}, { error: function(model, xhr, options) {
				new AlertView().render('alert-danger', xhr.status, xhr.responseJSON.error);
				model.fetch();
			}});
		},

		addAll: function()
		{
			var isNotMe = function(user) { 
				return user.get('user') != signedInUser; 
			};

			var otherUsers = this.collection.filter(isNotMe, this);

			var $friendList = this.$el.find('.list-group');
			$friendList.empty();
			_.each(otherUsers, function(user) {
				var view = new FriendListItem({model: this.model, user: user});
				$friendList.append(view.render().el);
			}, this);
		},

	});

	var RouteView = Backbone.View.extend({
		siteLocationTemplate: Handlebars.compile($('#site-location-template').html()),
		render: function(pages) {
	    	$("#site-location").html(this.siteLocationTemplate({ pages: pages }));
		}
	});

	var ProfileEditView = Backbone.View.extend({
		el: "#profile-edit-form",
		events: {
			"submit": "save",
			"click input[name=set-new-password]": "set_required",
		},
		initialize: function() {
			this.$inputs = {
				name: this.$el.find('input[name=name]'),
				email: this.$el.find('input[name=email]'),
				old_password: this.$el.find('input[name=old_password]'),
				new_password: this.$el.find('input[name=new_password]'),
				set_new_password: this.$el.find('input[name=set-new-password]'),
			};
			_.bindAll(this, "saveSuccess", "saveError");
			this.listenTo(this.model, "sync", this.render);
			SignedInUser.fetch();
		},
		set_required: function() {
			var checked = this.$inputs.set_new_password.prop('checked');

			this.$inputs.old_password.prop("required", checked);
			this.$inputs.new_password.prop("required", checked);
		},
		save: function(e) {
			e.preventDefault();

			var data = {
				name:             this.$inputs.name.val(),
				email:            this.$inputs.email.val(),
			};
			
			if(this.$inputs.set_new_password.prop('checked')) {
				data.password = this.$inputs.old_password.val();
				data.new_password = this.$inputs.new_password.val();
			}
			
			this.model.save(data, { 
				success: this.saveSuccess,
				error: this.saveError, 
				wait: true
			});

			this.$inputs.old_password.val("");
			this.$inputs.new_password.val("");
			this.$inputs.set_new_password.prop('checked', false);

			this.render();
		},
		saveSuccess: function() {
			var savebtn = this.$el.find('button:submit');
			savebtn.removeClass('btn-primary')
                   .addClass('btn-success')
                   .button('saved');
            setTimeout(function() {
	            savebtn.removeClass('btn-success')
	                   .addClass('btn-primary')
	                   .button('reset');
            }, 2000);
		},
		saveError: function(model, res) {
			new AlertView().render('alert-danger', res.status, res.responseJSON.error || "Error");
		},
		render: function() {
			this.$inputs.name.val(this.model.get('name'));
			this.$inputs.email.val(this.model.get('email'));
			return this;
		},
	});

	var ProfileDeleteView = Backbone.View.extend({
		el: "a#destroy-profile",
		events: {
			"click": "destroy",
		},
		initialize: function() {
			_.bindAll(this, "removeSuccess", "removeError");
		},
		destroy: function(e) {
			e.preventDefault();

			if(confirm("Delete profile " + this.model.get('user') + "?")) {
				this.model.destroy({ 
					success: this.removeSuccess,
					error: this.removeError, 
					wait: true
				});
			}
		},
		removeSuccess: function(model, response, options) {
			// go to profile page and reload to get signed in user status
			window.location.href = "/logout";
		},
		removeError: function(model, response, options) {
			new AlertView().render('alert-danger', res.status, res.responseJSON.error || "Error");
		},
	});

	var signedInUser = $("#signed-in-username").html();

	/* Collections */
	var AllUsers = new UserList();
	var Timeline = new StatusMessages();

	/* Models */
	var SelectedUser = new User;
	
	var SignedInUser;
	if(signedInUser) {
		SignedInUser = new User({user:signedInUser});
	} else {
		SignedInUser = new User;
	}

	/* Views */
	var TimelineView = new TimelineView({ collection: Timeline, model: SelectedUser });
	var CurrentRouteView = new RouteView;
	var SendStatusMessageView = new SendStatusMessageView;
	
	var UserListing = new UserListingView({ model: SignedInUser });
	var AddFriendModal = new FriendAddView({ collection: AllUsers, model: SignedInUser });

	var JoinModal = new JoinView();

	var AppRouter = Backbone.Router.extend({
	    routes:{
	        "":"list",
	        "settings":"settings",
	        ":username":"profile",
	    },

	    initialize: function() {
	    	AllUsers.fetch();
	    	Timeline.fetch();
	    },

	    list:function () {
	    	$('#main-page').show();
	    	$('#settings-page').hide();

	    	new AlertView().flash('alert-danger');

	    	CurrentRouteView.render([]);
	    	SendStatusMessageView.render(false);
	    	Timeline.limit = 5;
	    	SelectedUser.clear();
	    	UserListing.$el.show();
	    },

	    profile:function (username) {
	    	$('#main-page').show();
	    	$('#settings-page').hide();

	    	SendStatusMessageView.render(username == SignedInUser.get('user'));
	    	CurrentRouteView.render([{user: username}]);
	    	Timeline.limit = 0;
	    	SelectedUser.set({user:username});
	    	UserListing.$el.hide();
	    },

	    settings: function() {
	    	$('#main-page').hide();
	    	$('#settings-page').show();

	    	CurrentRouteView.render([{user: "Settings"}]);

	    	new ProfileEditView({model:SignedInUser});
	    	new ProfileDeleteView({model:SignedInUser});
	    },
	});

	setInterval(function() {
		Timeline.fetch();
	}, 10000);

	var App = new AppRouter();
	Backbone.history.start();

});

Handlebars.registerHelper('isOwnComment', function(user, owner) {
	return user === owner;
});

Handlebars.registerHelper('fromNow', function(timestamp) {
	return moment(timestamp).fromNow();
});
