/**
 * Module dependencies.
 */

var express = require('express');
var http = require('http');
var path = require('path');
var mongodb = require('mongodb');

var routes = require('./routes');
var api_user = require('./routes/api/user');
var api_status = require('./routes/api/status');
var db = require('./routes/api/db.js');
var requireAuth = require('./auth.js').requireAuth;

var config = require('./config.json');

var app = express();

// all environments
app.set('port', config.port || 3000);
app.set('database', config.mongodb || "mongodb://localhost/kamula");
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.cookieParser());
app.use(express.session({ secret: '934ee4518a2ddbe0cf74a2cfbb22bcb3' }));
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
	app.use(express.errorHandler());
}

mongodb.MongoClient.connect(app.get('database'), function(err, database) {
	if(err) {
		console.log("Could not commect to database " + app.get('database'));
		process.exit(1);
	}
	db.init(database);
	http.createServer(app).listen(app.get('port'), function() {
		console.log('Express server listening on port ' + app.get('port') + ' : ' + app.get('env'));
	});
});

/* Web pages */
app.get    ('/',       routes.index);
app.post   ('/login',  routes.login);
app.get    ('/logout', routes.logout);

/* REST API */
app.get    ('/api/users',                               api_user.list);
app.post   ('/api/users',                               api_user.create);
app.get    ('/api/users/:username',                     api_user.info);
app.put    ('/api/users/:username',        requireAuth, api_user.update);
app.delete ('/api/users/:username',        requireAuth, api_user.remove);

app.get    ('/api/statuses',                            api_status.timeline);
app.post   ('/api/statuses',               requireAuth, api_status.add);
app.get    (/^\/api\/statuses\/(\w{24})$/,              api_status.show);
app.delete (/^\/api\/statuses\/(\w{24})$/, requireAuth, api_status.remove);